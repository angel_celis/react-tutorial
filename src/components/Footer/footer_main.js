import React, { Component } from 'react';

export default class App extends Component {
  render() {

    const style_footer = {
      container: {
        backgroundColor:'#fafafa',
        padding: '0 20px',
        height: '70px',
        fontFamily:'proxima-nova, Helvetica Neue, Arial, Helvetica,sans-serif'
      },
      containerInnerFooter: {
        padding: '10px 0',
        textAlign: 'center',
        fontSize:'12px',
        fontWeight: '600',
        margin: '0 auto',
        textTransform: 'uppercase',
        width: '100%',
        height: '100%'
      },
      styleNav:{
        margin: '0 auto',
        maxWidth:'360px',
        height:'38px'
      },
      styleUl:
      {
        maxWidth:'360px',
        margin: '0 auto',
        listStyleType: 'none',
        padding: '0'
      },
      listStyle: {
        display: 'inline-block',
        marginRight: '16px',
      },
      copyright: {
        display: 'block',
        color: '#999',
        padding: '0px 15px 0px 0px'
      },
      anchor: {
        color: '#003569',
        textDecoration: 'none'
      }
    }

    return (
      <div style={style_footer.container}>
        <div style={style_footer.containerInnerFooter}>
          <nav style={style_footer.styleNav}>
            <ul style={style_footer.styleUl}>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>ABOUT US</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>SUPPORT</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>BLOG</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>PRESS</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>API</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>JOBS</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>PRIVACY</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>TERMS</a></li>
              <li style={style_footer.listStyle}><a href="#" style={style_footer.anchor}>LANGUAGE</a></li>
            </ul>
          </nav>
          <span style={style_footer.copyright}>&copy; 2016 INSTAGRAM</span>
        </div>
      </div>
    );
  }
}
