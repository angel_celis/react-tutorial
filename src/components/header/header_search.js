import React, { Component } from 'react';

export default class Logo extends Component {
  render() {
    const style_search ={
      box: {
      display: 'inline-block',
      height: '38px',
      width: '40%',
    },

     color: {
      backgroundColor: '#F9F8F6',
      textAlign: 'center',
    },


}

    return (
      <div style={style_search.box}>
        <input style={style_search.color} type="text" placeholder="Search" />
      </div>
    );
  }
}
