import React, { Component } from 'react';

import Username from './post_username';


export default class App extends Component {
  render() {
    const style_post = {
      main: {
        width:'100%',
        height:'2500px',
        backgroundColor: '#fafafa'
      },
      section: {
        margin: '0 auto',
        paddingTop: '60px',
        height:'100%',
        maxWidth: '640px',
      },
      addProfile: {
        padding:'10px',
        marginBottom:'20px',
        height: '160px',
        backgroundColor: 'white',
        border: '1px solid #efefef'
      },
      addPhoto: {
        margin:'0 auto',
        width:'140px',
        height:'135px',
        textAlign:'center'
      },
      suggestContainer: {
        padding:'0',
        marginBottom:'20px',
        height: '250px',
        backgroundColor: 'white',
        listStyleType:'none',
        border: '1px solid #efefef'
      },
      styleLi: {
        padding: '10px 20px',
        borderBottom: '1px solid #efefef'
      },
      styleLiB: {
        padding: '10px 20px 0px 20px',
      },
      styleH2: {
        display: 'inline-block',
        color: '#999',
        fontWeight: '600',
        fontSize:'14px',
        minWidth: '535px'
      },
      seeAll:{
        fontSize:'15px',
        fontWeight: '400',
        fontFamily: 'Arial'
      },

      profileName:{
        width:'450px',
        display:'inline-block',
        padding:'0px 0px 0px 10px',
        fontSize:'12px',
        fontWeight:'600'
      },
      postContent: {
          padding:'0',
          marginBottom:'20px',
          height: '845px',
          backgroundColor: 'white',
          border: '1px solid #efefef'
      },
      headerPost:{
        backgroundColor: 'white',
        border: '1px solid #efefef',
        fontSize: '15px',
        lineHeight: '18px',
        padding: '14px 20px'

      },
      likes: {
        padding: '0 24px'
      },
      sectionLikes: {
        marginTop:'20px',
        marginBottom:'7px'
      },
      instaTweets: {
        marginLeft: '-24px',
        marginRight: '-24px',
        paddingLeft: '24px',
        paddingRight: '24px',
        marginTop: '-5px',
        paddingTop: '5px',
        marginBottom: '7px',
        listStyleType: 'none',
      },
      comment: {
        minHeight:'60px',
        marginTop:'13px',
        borderTop: '1px solid #efefef',
        paddingTop:'20px',
        color: '#999',
      },
      placeholderComment: {
        border:'none',
        fontWeight:'100',
        minWidth: '479px'
      },
      report: {
        padding:'8px',
        marginLeft:'-9px',
        marginRight: '3px'

      },
      heart: {
        marginLeft: '40px',
        marginRight: '-10px',
        padding: '0'
      },
      username_image:  {
          width: '5%',
          margin: '0px',
      },
      username: {
          display: 'inline-block',
          width: '90%',
          lineHeight: '0',
          fontWeight:'600'
      },
      subcontent2image: {
          height: '610px',
          width: '100%',
          backgroundColor: 'grey',
          margin: '0',
          postion: 'absolute',
          display: 'block',
          bottom: '0',
          right: '0',
          position: 'relative',
          overflow: 'hidden',

      }
    }

    return (
      <div style={style_post.main}>
        <div style={style_post.section}>
          <div style={style_post.addProfile}>
            <div style={style_post.addPhoto}>
              <a href="#" style={style_post.picAdd}><img src="./images/picAdd.PNG" /></a>
              <p>Add Profile Photo</p>
            </div>
          </div>
          <ul style={style_post.suggestContainer}>
            <li style={style_post.styleLi}>
              <div style={style_post.suggest}>
              <h2 style={style_post.styleH2}>SUGGESTIONS FOR YOU</h2>
              <a href="#" style={style_post.seeAll}>See All &nbsp;></a>
              </div>
            </li>
            <li style={style_post.styleLi}>
              <a href="#" style={style_post.person1}><img src="./images/person1.PNG" /></a>
              <div style={style_post.profileName}>
                <div style={style_post.usernameProf}><a href="#" style={{color:'black'}}>jes.dc</a></div>
                <div style={style_post.nameProf}>Jesusa DC</div>
              </div>
              <a href="#" style={style_post.follow}><img src="./images/follow.PNG" /></a>
            </li>
            <li style={style_post.styleLi}>
              <div>
                <a href="#" style={style_post.person1}><img src="./images/person1.PNG" /></a>
                <div style={style_post.profileName}>
                  <div style={style_post.usernameProf}><a href="#" style={{color:'black'}}>jes.dc</a></div>
                  <div style={style_post.nameProf}>Jesusa DC</div>
                </div>
                <a href="#" style={style_post.follow}><img src="./images/follow.PNG" /></a>
              </div>
            </li>
            <li style={style_post.styleLiB}>
              <div>
                <a href="#" style={style_post.person1}><img src="./images/person1.PNG" /></a>
                <div style={style_post.profileName}>
                  <div style={style_post.usernameProf}><a href="#" style={{color:'black'}}>jes.dc</a></div>
                  <div style={style_post.nameProf}>Jesusa DC</div>
                </div>
                <a href="#" style={style_post.follow}><img src="./images/follow.PNG" /></a>
              </div>
            </li>
          </ul>

          <div style={style_post.postContent}>
            <div style={style_post.headerPost}>
              <img style={style_post.username_image} src ={'url(https://instagramstatic-a.akamaihd.net/h1/sprites/core/ce49c5.png)'} />
              <div style={style_post.username}>
                Usernames
              </div>
              <a href="#" > #h </a>
            </div>
            <div>
             <img style={style_post.subcontent2image} src ={'url()'} />
            </div>
              <div style={style_post.likes}>
                <section style={style_post.sectionLikes}>
                  <div><strong style={{fontWeight:'600', color:'black', display:'inline-block'}}>14</strong> Likes</div>
                </section>
                  <section>
                    <ul style={style_post.instaTweets}>
                      <li><a href="#" style={{fontWeight:'600', color:'black'}}>shinatwentyone</a> Thank you for spending time with me. 💕</li>
                      <li><a href="#" style={{fontWeight:'600', color:'black'}}>lovelygracee</a> maganda ate? <a href="#" style={{color:'black'}}>@shinatwentyone</a></li>
                    </ul>
                  </section>
                <section style={style_post.comment}>
                  <a href="#" style={style_post.report}><img src="./images/heart.PNG" /></a>
                  <input type="text" placeholder="Add a comment..." style={style_post.placeholderComment}/>
                  <a href="#" style={style_post.heart}><img src="./images/report.PNG" /></a>
                </section>
              </div>
          </div>
          </div>
        </div>


    );
  }
}
