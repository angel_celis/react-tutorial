import React, { Component } from 'react';

import Profilephoto from './post_add_Profilephoto';
import Suggestions from './post_suggestions';


export default class App extends Component {
  render() {
    const style_container = {
      container: {
      //  display: 'block',
        margin: '0 auto',
        paddingTop: '60px',
        height:'100%',
        maxWidth: '640px',
      },
      addProfile: {
        padding:'10px',
        marginBottom:'20px',
        height: '160px',
        backgroundColor: 'white',
        border: '1px solid #efefef'
      },
      addPhoto: {
        margin:'0 auto',
        width:'140px',
        height:'135px',
        textAlign:'center'
      },
      suggestContainer: {
        padding:'0',
        marginBottom:'20px',
        height: '230px',
        backgroundColor: 'white',
        listStyleType:'none',
        marginBottom:'20px',
        border: '1px solid #efefef'
      },
      styleLi: {
        padding: '16px 20px',
        borderBottom: '1px solid #efefef'
      },
      styleLiB: {
        padding: '16px 20px',
      },
      styleH2: {
        display: 'inline-block',
        color: '#999',
        fontWeight: '600',
        fontSize:'14px'
      },
      postContent: {
        padding:'0',
        marginBottom:'20px',
        height: '230px',
        backgroundColor: 'white',
        listStyleType:'none',
        marginBottom:'20px',
        border: '1px solid #efefef'
      }
    }
    return (
      <div style={style_post.main}>
        <div style={style_post.section}>
          <div style={style_post.addProfile}>
            <div style={style_post.addPhoto}>
            <p>Add Profile Photo</p>
            </div>
          </div>
          <ul style={style_post.suggestContainer}>
            <li style={style_post.styleLi}>
              <div style={style_post.suggest}>
              <h2 style={style_post.styleH2}>SUGGESTIONS FOR YOU</h2>
              <a href="#" style={style_post.seeAll}>See All &nbsp;></a>
              </div>
            </li>
            <li style={style_post.styleLi}>
              <div>
              Person1
              <a href="#">Follow</a>
              </div>
            </li>
            <li style={style_post.styleLi}>
              <div>
              Person2
              <a href="#">Follow</a>
              </div>
            </li>
            <li style={style_post.styleLiB}>
              <div>
              Person3
              <a href="#">Follow</a>
              </div>
            </li>
          </ul>
          <div stylye={style_post.postContent}>
          Profile
          </div>
        </div>
      </div>
    );
  }
}
